package sbu.cs.exception;

import java.util.ArrayList;
import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public void readTwitterCommands(List<String> args) throws ApException {
        for (String cm: args) {
            if(Util.getNotImplementedCommands().contains(cm))
                throw new NotImplementedCommand();
            else if( !Util.getImplementedCommands().contains(cm))
                throw new UnrecognizedCommand();
        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public void read(String...args) throws BadInputException {
        String[] str = args;
        for( int i = 1; i < str.length; i += 2 ){
            try{
                Integer.parseInt(str[i]);
            }
            catch (NumberFormatException e){
                throw new BadInputException();
            }
        }
    }
}
